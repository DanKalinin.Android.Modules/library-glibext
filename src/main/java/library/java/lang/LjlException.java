package library.java.lang;

public class LjlException extends Exception {
    public LjlException(String message) {
        super(message);
    }
}
