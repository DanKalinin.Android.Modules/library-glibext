package library.android.media;

import android.media.AudioRecord;

public class LamAudioRecord extends AudioRecord {
    public LamAudioRecord(int audioSource, int sampleRateInHz, int channelConfig, int audioFormat, int bufferSizeInBytes) throws IllegalArgumentException {
        super(audioSource, sampleRateInHz, channelConfig, audioFormat, bufferSizeInBytes);
    }
}
