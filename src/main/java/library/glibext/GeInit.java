package library.glibext;

import android.content.Context;
import androidx.startup.Initializer;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import library.java.lang.LjlObject;

public class GeInit extends LjlObject implements Initializer<Void> {
    @Override
    public Void create(Context context) {
        System.loadLibrary("library-glibext");
        l10n(Locale.getDefault().getLanguage());
        tz(TimeZone.getDefault().getRawOffset() / 1000);
        schemas(context.getFilesDir().toString());
        cwd(context.getFilesDir().toString());
        return null;
    }

    @Override
    public List<Class<? extends Initializer<?>>> dependencies() {
        ArrayList<Class<? extends Initializer<?>>> ret = new ArrayList<>();
        return ret;
    }

    public static native void l10n(String language);
    public static native void tz(int seconds);
    public static native void schemas(String directory);
    public static native void cwd(String directory);
}
