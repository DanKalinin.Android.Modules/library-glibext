package library.glibext;

import library.java.lang.LjlException;

public class GeException extends LjlException {
    public String domain;
    public int code;

    public GeException(String domain, int code, String message) {
        super(message);

        this.domain = domain;
        this.code = code;
    }
}
