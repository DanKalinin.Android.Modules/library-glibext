//
// Created by Dan on 14.10.2021.
//

#include "GeException.h"

GeExceptionType GeException = {0};

void GeExceptionLoad(JNIEnv *env) {
    jclass class = (*env)->FindClass(env, "library/glibext/GeException");
    GeException.class = (*env)->NewGlobalRef(env, class);
    GeException.init = (*env)->GetMethodID(env, class, "<init>", "(Ljava/lang/String;ILjava/lang/String;)V");
}

void GeExceptionUnload(JNIEnv *env) {
    (*env)->DeleteGlobalRef(env, GeException.class);
}

jthrowable GeExceptionFrom(JNIEnv *env, GError *sdkError) {
    gchar *sdkDomain = (gchar *)g_quark_to_string(sdkError->domain);
    jstring domain = (*env)->NewStringUTF(env, sdkDomain);
    jstring message = (*env)->NewStringUTF(env, sdkError->message);
    jthrowable this = (*env)->NewObject(env, GeException.class, GeException.init, domain, sdkError->code, message);
    return this;
}

jint GeExceptionThrowFrom(JNIEnv *env, GError *sdkError) {
    jthrowable this = GeExceptionFrom(env, sdkError);
    jint ret = (*env)->Throw(env, this);
    return ret;
}
