//
// Created by Dan on 18.10.2021.
//

#ifndef LIBRARY_GLIBEXT_GEINIT_H
#define LIBRARY_GLIBEXT_GEINIT_H

#include "GeMain.h"
#include "GeJNI.h"
#include "GeException.h"

G_BEGIN_DECLS

extern JavaVM *GeJavaVM;

G_END_DECLS

#endif //LIBRARY_GLIBEXT_GEINIT_H
