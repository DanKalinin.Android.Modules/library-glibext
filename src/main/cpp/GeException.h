//
// Created by Dan on 14.10.2021.
//

#ifndef LIBRARY_GLIBEXT_GEEXCEPTION_H
#define LIBRARY_GLIBEXT_GEEXCEPTION_H

#include "GeMain.h"

G_BEGIN_DECLS

typedef struct {
    jclass class;
    jmethodID init;
} GeExceptionType;

extern GeExceptionType GeException;

void GeExceptionLoad(JNIEnv *env);
void GeExceptionUnload(JNIEnv *env);

jthrowable GeExceptionFrom(JNIEnv *env, GError *sdkError);
jint GeExceptionThrowFrom(JNIEnv *env, GError *sdkError);

G_END_DECLS

#endif //LIBRARY_GLIBEXT_GEEXCEPTION_H
