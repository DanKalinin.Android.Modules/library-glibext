//
// Created by Dan on 18.10.2021.
//

#include "GeInit.h"

JavaVM *GeJavaVM = NULL;

jint JNI_OnLoad(JavaVM *vm, gpointer reserved) {
    ge_init();

    GeJavaVM = vm;

    JNIEnv *env = NULL;
    (void)(*vm)->GetEnv(vm, (gpointer *)&env, JNI_VERSION_1_2);

    GeExceptionLoad(env);

    return JNI_VERSION_1_2;
}

void JNI_OnUnload(JavaVM *vm, gpointer reserved) {
    JNIEnv *env = NULL;
    (void)(*vm)->GetEnv(vm, (gpointer *)&env, JNI_VERSION_1_2);

    GeExceptionUnload(env);
}

JNIEXPORT void JNICALL Java_library_glibext_GeInit_l10n(JNIEnv *env, jclass class, jstring language) {
    gchar *sdkLanguage = GeJNIEnvGetStringUTFChars(env, language, NULL);
    ge_init_l10n(sdkLanguage);
    GeJNIEnvReleaseStringUTFChars(env, language, sdkLanguage);
}

JNIEXPORT void JNICALL Java_library_glibext_GeInit_tz(JNIEnv *env, jclass class, jint seconds) {
    ge_init_tz(seconds);
}

JNIEXPORT void JNICALL Java_library_glibext_GeInit_schemas(JNIEnv *env, jclass class, jstring directory) {
    gchar *sdkDirectory = GeJNIEnvGetStringUTFChars(env, directory, NULL);
    ge_init_schemas(sdkDirectory);
    GeJNIEnvReleaseStringUTFChars(env, directory, sdkDirectory);
}

JNIEXPORT void JNICALL Java_library_glibext_GeInit_cwd(JNIEnv *env, jclass class, jstring directory) {
    gchar *sdkDirectory = GeJNIEnvGetStringUTFChars(env, directory, NULL);
    ge_init_cwd(sdkDirectory);
    GeJNIEnvReleaseStringUTFChars(env, directory, sdkDirectory);
}
