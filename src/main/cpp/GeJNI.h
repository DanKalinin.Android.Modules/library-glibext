//
// Created by Dan on 18.10.2021.
//

#ifndef LIBRARY_GLIBEXT_GEJNI_H
#define LIBRARY_GLIBEXT_GEJNI_H

#include "GeMain.h"

G_BEGIN_DECLS

void GeJNIEnvCallVoidMethod(JNIEnv *env, jobject this, jmethodID methodID, ...);
void GeJNIEnvCallVoidMethodV(JNIEnv *env, jobject this, jmethodID methodID, va_list args);

jlong GeJNIEnvGetLongField(JNIEnv *env, jobject this, jfieldID fieldID);

gchar *GeJNIEnvGetStringUTFChars(JNIEnv *env, jstring this, jboolean *isCopy);
void GeJNIEnvReleaseStringUTFChars(JNIEnv *env, jstring this, gchar *utf);

jobjectArray GeJNIEnvJObjectArrayGListGCharArrayFrom(JNIEnv *env, GList *sdkValues);
GList *GeJNIEnvJObjectArrayGListGCharArrayTo(JNIEnv *env, jobjectArray this);
GStrv GeJNIEnvJObjectArrayGStrvTo(JNIEnv *env, jobjectArray this);

jintArray GeJNIEnvJIntArrayGListGIntFrom(JNIEnv *env, GList *sdkValues);
GList *GeJNIEnvJIntArrayGListGIntTo(JNIEnv *env, jintArray this);

G_END_DECLS

#endif //LIBRARY_GLIBEXT_GEJNI_H
