//
// Created by Dan on 18.10.2021.
//

#include "GeJNI.h"

void GeJNIEnvCallVoidMethod(JNIEnv *env, jobject this, jmethodID methodID, ...) {
    va_list args;
    va_start(args, methodID);
    GeJNIEnvCallVoidMethodV(env, this, methodID, args);
    va_end(args);
}

void GeJNIEnvCallVoidMethodV(JNIEnv *env, jobject this, jmethodID methodID, va_list args) {
    if (this == NULL) return;
    (*env)->CallVoidMethodV(env, this, methodID, args);
}

jlong GeJNIEnvGetLongField(JNIEnv *env, jobject this, jfieldID fieldID) {
    if (this == NULL) return 0;
    jlong ret = (*env)->GetLongField(env, this, fieldID);
    return ret;
}

gchar *GeJNIEnvGetStringUTFChars(JNIEnv *env, jstring this, jboolean *isCopy) {
    if (this == NULL) return NULL;
    gchar *ret = (gchar *)(*env)->GetStringUTFChars(env, this, isCopy);
    return ret;
}

void GeJNIEnvReleaseStringUTFChars(JNIEnv *env, jstring this, gchar *utf) {
    if (this == NULL) return;
    (*env)->ReleaseStringUTFChars(env, this, utf);
}

jobjectArray GeJNIEnvJObjectArrayGListGCharArrayFrom(JNIEnv *env, GList *sdkValues) {
    guint n = g_list_length(sdkValues);
    jclass class = (*env)->FindClass(env, "java/lang/String");
    jobjectArray ret = (*env)->NewObjectArray(env, n, class, NULL);
    jsize i = 0;

    for (GList *sdkValue = sdkValues; sdkValue != NULL; sdkValue = sdkValue->next) {
        jstring value = (*env)->NewStringUTF(env, sdkValue->data);
        (*env)->SetObjectArrayElement(env, ret, i, value);
        i++;
    }

    return ret;
}

GList *GeJNIEnvJObjectArrayGListGCharArrayTo(JNIEnv *env, jobjectArray this) {
    if (this == NULL) return NULL;

    jsize n = (*env)->GetArrayLength(env, this);
    GList *ret = NULL;

    for (jsize i = 0; i < n; i++) {
        jstring value = (*env)->GetObjectArrayElement(env, this, i);
        gchar *sdkValue = GeJNIEnvGetStringUTFChars(env, value, NULL);
        ret = g_list_append(ret, g_strdup(sdkValue));
        GeJNIEnvReleaseStringUTFChars(env, value, sdkValue);
    }

    return ret;
}

GStrv GeJNIEnvJObjectArrayGStrvTo(JNIEnv *env, jobjectArray this) {
    if (this == NULL) return NULL;

    jsize n = (*env)->GetArrayLength(env, this);
    GStrv ret = g_new0(gchar *, n + 1);

    for (jsize i = 0; i < n; i++) {
        jstring value = (*env)->GetObjectArrayElement(env, this, i);
        gchar *sdkValue = GeJNIEnvGetStringUTFChars(env, value, NULL);
        ret[i] = g_strdup(sdkValue);
        GeJNIEnvReleaseStringUTFChars(env, value, sdkValue);
    }

    return ret;
}

jintArray GeJNIEnvJIntArrayGListGIntFrom(JNIEnv *env, GList *sdkValues) {
    guint n = g_list_length(sdkValues);
    jintArray ret = (*env)->NewIntArray(env, n);
    jsize i = 0;

    for (GList *sdkValue = sdkValues; sdkValue != NULL; sdkValue = sdkValue->next) {
        (*env)->SetIntArrayRegion(env, ret, i, 1, (jint *)&sdkValue->data);
        i++;
    }

    return ret;
}

GList *GeJNIEnvJIntArrayGListGIntTo(JNIEnv *env, jintArray this) {
    if (this == NULL) return NULL;

    jsize n = (*env)->GetArrayLength(env, this);
    GList *ret = NULL;

    for (jsize i = 0; i < n; i++) {
        jint value = 0;
        (*env)->GetIntArrayRegion(env, this, i, 1, &value);
        ret = g_list_append(ret, GINT_TO_POINTER(value));
    }

    return ret;
}
